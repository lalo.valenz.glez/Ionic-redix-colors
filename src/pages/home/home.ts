import { DevToolsExtension } from '@angular-redux/store/lib/src/components/dev-tools';
import { NgRedux } from '@angular-redux/store/lib/src/components/ng-redux';
import { ColorListState } from '../../models/color.model';
import { Observable } from 'rxjs/Rx';
import { select } from '@angular-redux/store';
import { dispatch } from '@angular-redux/store';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ColorActions, ColorAddAction, ColorLoadAction, ColorRemoveAction } from '../../actions/color.action';
import { colorReducer, INITIAL_STATE } from '../../reducers/color.reducer';

declare var require;

var reduxLogger = require('redux-logger');

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    @select() readonly colorList$: Observable<ColorListState>

    constructor(public store: NgRedux<ColorListState>, public colorActions: ColorActions, public devTools: DevToolsExtension) {
        store.configureStore(
            colorReducer,
            INITIAL_STATE,
           [reduxLogger.createLogger()],
           devTools.isEnabled ? [devTools.enhancer() ] : []);

           colorActions.loadColors();
    }

    addColors(){
        this.colorActions.addColors([`rgb(${Math.round(Math.random() * 255) },${Math.round(Math.random() * 255) },${Math.round(Math.random() * 255) })`]);
    }

    removeLastColor(){
        this.colorActions.removeLastColor();
    }

}
